<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('guest/index');
});

Route::get('don-hang', function () {
    return view('guest/order');
});

Route::get('chi-tiet-don-hang', function () {
    return view('guest/order-details');
});

Route::get('tin-tuc', function () {
    return view('guest/news');
});

Route::get('lien-he', function () {
    return view('guest/contact-us');
});

@extends('guest/default')
@section('content')  
@include('guest/navbar')
  <!-- ======= Hero Section ======= -->
  <section id="hero5" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      {{-- <h1>Liên <span>hệ</span></h1> --}}
    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Liên hệ</h2>
          <h3><span>Liên hệ chúng tôi</span></h3>
          <p>Đánh giá và gửi ý kiến phản hồi cho chúng tôi để cùng hợp tác và phát triển.</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3>Địa chỉ</h3>
              <p>87 Nguyễn Thị Minh Khai, TP.Vinh, Nghệ AN</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>
              <h3>Email</h3>
              <p>duhocvn@example.com</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Số điện thoại</h3>
              <p>+84 912345678</p>
            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">

          <div class="col-lg-6 ">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3779.7656411338885!2d105.67623671439041!3d18.674509769323215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139ce706fd0344b%3A0x6f0b06a681d64b03!2zODcgTmd1eeG7hW4gVGjhu4sgTWluaCBLaGFpLCBIxrBuZyBCw6xuaCwgVGjDoG5oIHBo4buRIFZpbmgsIE5naOG7hyBBbiwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1605776228550!5m2!1svi!2s" width="100%" height="368" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>

          <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">
                <div class="col form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Họ và tên" data-rule="minlen:4" data-msg="Nhập ít nhất 4 ký tự" />
                  <div class="validate"></div>
                </div>
                <div class="col form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Địa chỉ email" data-rule="email" data-msg="Nhập đúng định dạng email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Tiêu đề" data-rule="minlen:4" data-msg="Nhập ít nhất 8 ký tự" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Đưa ra một vài ý kiến cho chúng tôi" placeholder="Nội dung phản hồi"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Đang tải</div>
                <div class="error-message"></div>
                <div class="sent-message">Tin nhắn của bạn đã được gửi. Cảm ơn!</div>
              </div>
              <div class="text-center"><button type="submit">Gửi</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->
@include('guest/footer')
@endsection
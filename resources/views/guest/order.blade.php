@extends('guest/default')
@section('content')  
@include('guest/navbar')
  <!-- ======= Hero Section ======= -->
  <section id="hero2" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      {{-- <h1>Liên <span>hệ</span></h1> --}}
    </div>
  </section><!-- End Hero -->

  <main id="main">
      
      <!-- ======= Oders Section ======= -->
      <section id="orders" class="orders">

        <div class="section-title">
          <h2>Du học Nhật Bản</h2>
       </div>

        <div class="container d-flex">
          <div class="row" data-aos="zoom-out">
  
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3">
              <div class="d-flex align-items-start orders-db">
                <div class="pic col-lg-3 col-sm-12"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-lg-9 col-sm-12">
                  <h4><a href="chi-tiet-don-hang">[Tuyển sinh] Du học Nhật Bản mới nhất</a></h4>
                  <p class="justify-text">Du học Nhật Bản là một trong những quyết định bước ngoặt thay đổi tương lai, do đó bạn nên tìm hiểu kỹ và lựa chọn trung tâm du học uy tín cũng như mục tiêu học ngành nghề gì, học trường nào ở Nhật… Những bước đi ban đầu bao giờ cũng khó khăn, nhất là đối với việc học một ngôn ngữ được cho là...</p>
                  <a class="btn btn-primary color-fff pull-right" href="chi-tiet-don-hang">Xem thêm</a>
                </div>
              </div>
            </div>
  
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3">
              <div class="d-flex align-items-start orders-db">
                <div class="pic col-lg-3 col-sm-12"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-lg-9 col-sm-12">
                  <h4><a href="chi-tiet-don-hang">Thông báo tuyển sinh du học Nhật Bản</a></h4>
                  <p class="justify-text">Thông tin mới cho các bạn trẻ đang tìm hiểu về chương trình “Du học Nhật Bản”. Đây là chương trình nhận được rất nhiều sự quan tâm từ các bạn học sinh, sinh viên bởi được học tập hoặc làm việc ở một đất nước văn minh như Nhật Bản quả thật không còn gì tốt hơn. Vậy thì chúng ta hãy cùng tìm hiểu tất...</p>
                  <a class="btn btn-primary color-fff pull-right" href="chi-tiet-don-hang">Xem thêm</a>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3">
              <div class="d-flex align-items-start orders-db">
                <div class="pic col-lg-3 col-sm-12"><img src="./img/update3.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-lg-9 col-sm-12">
                  <h4><a href="chi-tiet-don-hang">Tuyển sinh du học Nhật Bản kỳ tháng 10</a></h4>
                  <p class="justify-text">Du học Nhật Bản kỳ tháng 10 có điểm gì mới? Những điểm cần lưu ý khi đăng ký kỳ học này là gì? Cùng tìm hiểu những đặc điểm của du học Nhật Bản kỳ tháng 10 ngay trong bài viết ngày hôm nay để có sự chuẩn bị tốt nhất cho hành trang du học của mình. Tuyển sinh du học Nhật Bản kỳ tháng 10 Đối tượng tham...</p>
                  <a class="btn btn-primary color-fff pull-right" href="chi-tiet-don-hang">Xem thêm</a>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3">
              <div class="d-flex align-items-start orders-db">
                <div class="pic col-lg-3 col-sm-12"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-lg-9 col-sm-12">
                  <h4><a href="chi-tiet-don-hang">Thông báo Kỳ thi Du học Nhật Bản (EJU) đợt 2 năm 2020</a></h4>
                  <p class="justify-text">Thông báo mới dành cho các bạn đi du học nhật bản 2020 đây. Kỳ thi EJU do Hỗ trợ sinh viên Nhật Bản JASSO tổ chức 1 năm 2 lần. Kỳ thi Du học Nhật Bản (EJU) đợt 2 năm 2020 EJU là kỳ thi được tổ chức với mục đích đánh giá năng lực tiếng Nhật và trình độ kiến thức cơ bản của du học sinh người nước ngoài muốn theo...</p>
                  <a class="btn btn-primary color-fff pull-right" href="chi-tiet-don-hang">Xem thêm</a>
                </div>
              </div>
            </div>


            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3">
              <div class="d-flex align-items-start orders-db">
                <div class="pic col-lg-3 col-sm-12"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-lg-9 col-sm-12">
                  <h4><a href="chi-tiet-don-hang">Tuyển sinh du học Nhật Bản kỳ tháng 10/2020</a></h4>
                  <p class="justify-text">Các bạn đang có dự định đi du học Nhật Bản cuối năm nay, sau khi hoàn thành xong kỳ thi Đại Học hay sau một thời gian đi làm ? Hãy cùng chúng mình tìm hiểu xem du học Nhật Bản kỳ cuối năm có những đặc điểm và điều kiện như thế nào ngay dưới đây nhé !   DU HỌC NHẬT BẢN THÁNG 10 VÀ NHỮNG ĐIỀU CẦN BIẾT   1....</p>
                  <a class="btn btn-primary color-fff pull-right" href="chi-tiet-don-hang">Xem thêm</a>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3">
              <div class="d-flex align-items-start orders-db">
                <div class="pic col-lg-3 col-sm-12"><img src="./img/update3.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-lg-9 col-sm-12">
                  <h4><a href="chi-tiet-don-hang">Năm Châu IMS tuyển sinh du học Nhật Bản 2020 với nhiều ưu đãi hấp dẫn</a></h4>
                  <p class="justify-text">Nhật Bản là một đất nước phát triển, các bạn sẽ có cơ hội phát triển bản thân mình hơn. Du học Nhật Bản hiện đang là sự lựa chọn của rất nhiều bạn trẻ. Năm Châu IMS là công ty tuyển sinh du học Nhật Bản nhận được rất nhiều sự ủng hộ của các bạn trẻ. Mùa tuyển sinh du học Nhật Bản 2020 các kỳ tháng...</p>
                  <a class="btn btn-primary color-fff pull-right" href="chi-tiet-don-hang">Xem thêm</a>
                </div>
              </div>
            </div>
  
          </div>
        </div>
      </section><!-- End Orders Section -->

  </main><!-- End #main -->
@include('guest/footer')
@endsection
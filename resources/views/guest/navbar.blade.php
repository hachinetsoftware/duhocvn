  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto:duhocvn@example.com" data-toggle="tooltip" data-placement="bottom" title="duhocvn@example.com">duhocvn@example.com</a>
        <i class="icofont-phone"></i> <span data-toggle="tooltip" data-placement="bottom" title="+84 912345678"><a href="tel:+84 912345678">+84 912345678</a></span>
      </div>
      <div class="social-links">
        <a href="#" class="twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype" data-toggle="tooltip" data-placement="bottom" title="Skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin" data-toggle="tooltip" data-placement="bottom" title="LinkedIn"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href=".">DuhocVN</a></h1>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li><a href="." data-toggle="tooltip" data-placement="top" title="Trang chủ">Trang chủ</a></li>
          <li class="drop-down" data-toggle="tooltip" data-placement="top" title="Đơn hàng"><a href="#">Đơn hàng</a>
            <ul>
              <li><a href="don-hang" data-toggle="tooltip" data-placement="top" title="Du học Nhật Bản">Du học Nhật Bản</a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="Du học Đức">Du học Đức</a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="Du học Mỹ">Du học Mỹ</a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="top" title="Du học Canada">Du học Canada</a></li>
            </ul>
          </li>
          <li><a href="tin-tuc" data-toggle="tooltip" data-placement="top" title="Tin tức">Tin tức</a></li>
          <li><a href="lien-he" data-toggle="tooltip" data-placement="top" title="Liên hệ">Liên hệ</a></li>
          {{-- <li class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Deep Drop Down</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li> --}}

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
@extends('guest/default')
@section('content')  
@include('guest/navbar')
  <!-- ======= Hero Section ======= -->
  <section id="hero3" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      {{-- <h1>Liên <span>hệ</span></h1> --}}
    </div>
  </section><!-- End Hero -->

  <main id="main">
      
      <!-- ======= Order Detail Section ======= -->
      <section id="order-detail" class="order-detail">

        <div class="section-title">
          <h3><span>[TUYỂN SINH] DU HỌC NHẬT BẢN MỚI NHẤT</span></h3>
          <hr>
       </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
              <div class="member-info col-sm-10 m-auto" data-aos="zoom-out">
                <p>Du học Nhật Bản là một trong những quyết định bước ngoặt thay đổi tương lai, do đó bạn nên tìm hiểu kỹ và lựa chọn trung tâm du học uy tín cũng như mục tiêu học ngành nghề gì, học trường nào ở Nhật…</p>

                <p>Những bước đi ban đầu bao giờ cũng khó khăn, nhất là đối với việc học một ngôn ngữ được cho là khó nhất nhì thế giới. Nhưng bạn đừng vội lo lắng và nản chí, Năm Châu IMS với nhiều năm kinh nghiệm đào tạo tiếng Nhật và đã dẫn dắt rất nhiều bạn trẻ Việt Nam thành công trên con đường du học.</p>

                <h2>CHƯƠNG TRÌNH TUYỂN SINH DU HỌC NHẬT BẢN MỚI NHẤT</h2>

                <div class="text-center m-5" data-aos="zoom-out"><img src="./img/orderdetail1.jpg" class="img-fluid" alt=""></div>

                <ul class="p-0" data-aos="zoom-out">
                  <h4>Những lợi thế khi du học ở Nhật Bản</h4>  
                  <li class="pl-3">Được học tập tại nước phát triển có nền kinh tế đứng trong top đầu thế giới.</li>
                  <li class="pl-3">Cơ hội giao lưu với bạn bè trên khắp thế giới</li>
                  <li class="pl-3">Thăm quan và du lịch đất nước Nhật Bản</li>
                  <li class="pl-3">Có bằng cấp giá trị</li>
                  <li class="pl-3">Cơ hội làm việc lâu dài tại Nhật Bản thu nhập cao</li>
                  <li class="pl-3">Hoặc về Việt Nam xin việc thu nhập hấp dẫn</li>
                </ul>

                <ul class="p-0" data-aos="zoom-out">
                  <h4>Đối tượng tuyển sinh du học Nhật Bản</h4>  
                  <li class="pl-3">Nam/nữ tuổi từ 18-30</li>
                  <li class="pl-3">Tốt nghiệp cấp 3, CĐ, ĐH… Năm tốt nghiệp bằng cấp cao nhất không quá 5 năm.</li>
                  <li class="pl-3">Có đủ điều kiện tài chính đi du học</li>
                  <li class="pl-3">Tiếng Nhật chưa yêu cầu (sẽ được trung tâm đào tạo)</li>
                </ul>

                <ul class="p-0" data-aos="zoom-out">
                  <h4>Kỳ tuyển sinh</h4>  
                  <li class="pl-3">Du học Nhật bản tháng 1</li>
                  <li class="pl-3">Du học Nhật bản tháng 4</li>
                  <li class="pl-3">Du học Nhật bản tháng 7</li>
                  <li class="pl-3">Du học Nhật bản tháng 10</li>
                </ul>

                <ul class="p-0" data-aos="zoom-out">
                  <h4>Chi phí du học nhật</h4>  
                  <li class="pl-3">Du học sinh phải đóng học phí và tiền ký túc xá cho nhà trường tại Nhật Bản</li>
                  <li class="pl-3">Tiền chi tiêu sinh hoạt khi ở bên Nhật</li>
                  <li class="pl-3">Tiền ăn ở học tiếng tại Việt Nam</li>
                  <li class="pl-3">Tiền dịch vụ, hồ sơ tại Việt Nam</li>
                </ul>

                <p>Tổng chi phí du học nhật của học sinh tùy thuộc vào trường các em lựa chọn. Nếu các em đăng ký vào trường ở khu vực thành phố lớn… học phí sẽ cao hơn các trường tỉnh lẻ do đó gia đình phải tính toán kỹ trước khi tham gia. Khi tới DuhocVN, các bậc phụ huynh và các em học sinh sẽ được tư vấn hướng dẫn chi tiết nhất, mọi chi phí du học sẽ được trung tâm thông báo minh bạch rõ ràng nên 100% gia đình học sinh đều yên tâm.</p>

                <div class="text-center m-5" data-aos="zoom-out"><img src="./img/orderdetail2.jpg" class="img-fluid" alt=""></div>

                <h5>KHI ĐĂNG KÝ DU HỌC TẠI DUHOCVN, CÁC BẠN HỌC SINH SẼ ĐƯỢC ĐẢM BẢO:</h5>

                <p>Được tư vấn miễn phí 100% về trường học, chương trình học, học phí của các trường tại Nhật Bản.</p>

                <p>Được chọn trường theo đúng nguyện vọng.</p>

                <p>Đảm bảo hồ sơ du học chuyên nghiệp, chính xác, đáp ứng được tất cả yêu cầu của nhà trường và Đại Sứ Quán Nhật Bản với mức chi phí thấp nhất.</p>

                <p>Tỷ lệ đỗ visa đạt 99,9%.</p>

                <p>Có trung tâm đào tạo tiếng Nhật riêng biệt, phòng học, ký túc xá khang trang hiện đại theo tiêu chuẩn Nhật Bản.</p>

                <p>DuhocVN có văn phòng đại diện tại Nhật Bản để trực tiếp hỗ trợ các em học sinh.</p>

                <p>Du học sinh sẽ có việc làm thêm sau khi sang Nhật khoảng 3 tuần.</p>

              </div>
            </div>
            <hr>
          </div>
          <div class="row">
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
              <div class="fb-comments" data-href="https://www.hachinet.vn" data-width="100%" data-numposts="5"></div>
            </div>
          </div>
        </div>
      </section><!-- End Order Detail Section -->

  </main><!-- End #main -->
  <div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=323092058795310&autoLogAppEvents=1" nonce="xyoHlCRL"></script>
@include('guest/footer')
@endsection
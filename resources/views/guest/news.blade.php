@extends('guest/default')
@section('content')  
@include('guest/navbar')
  <!-- ======= Hero Section ======= -->
  <section id="hero4" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      {{-- <h1>Liên <span>hệ</span></h1> --}}
    </div>
  </section><!-- End Hero -->

  <main id="main">
      
      <!-- ======= News Section ======= -->
      <section id="news" class="news">

        <div class="section-title">
          <h2>Tin tức</h2>
          <h3>Tin tức về du học</h3>
       </div>

        <div class="container d-flex">
          <div class="member col-lg-8 col-sm-8 col-xs-12">
  
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
              <div class="d-flex align-items-start">
                <div class="pic col-sm-5"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-sm-7">
                  <h4>Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</h4>
                  <span>20/11/2020</span>
                  <p>Trả lời phỏng vấn của cục xuất nhập cảnh (XNC) Nhật Bản là bước rất Trả lời phỏng vấn của cục xuất nhập cảnh (XNC) Nhật Bản là bước rất Trả lời phỏng vấn của cục xuất nhập cảnh (XNC) Nhật Bản là bước rất Trả lời phỏng vấn của cục xuất nhập cảnh (XNC) Nhật Bản là bước rất ...</p>
                </div>
              </div>
            </div>
  
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
              <div class="d-flex align-items-start">
                <div class="pic col-sm-5"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-sm-7">
                  <h4>Điều kiện du học Nhật Bản 2019</h4>
                  <span>20/11/2020</span>
                  <p>I – ĐIỀU KIỆN DU HỌC NHẬT BẢN CHƯƠNG TRÌNH THÔNG THƯỜNG 1 – DU I – ĐIỀU KIỆN DU HỌC NHẬT BẢN CHƯƠNG TRÌNH THÔNG THƯỜNG 1 – DUI – ĐIỀU KIỆN DU HỌC NHẬT BẢN CHƯƠNG TRÌNH THÔNG THƯỜNG 1 – DUI – ĐIỀU KIỆN DU HỌC NHẬT BẢN CHƯƠNG TRÌNH THÔNG THƯỜNG 1 – DUI – ĐIỀU KIỆN DU HỌC NHẬT BẢN CHƯƠNG TRÌNH THÔNG THƯỜNG 1 – DU ...</p>
                </div>
              </div>
            </div>
  
            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
              <div class="d-flex align-items-start">
                <div class="pic col-sm-5"><img src="./img/update3.jpg" class="img-fluid" alt=""></div>
                <div class="member-info col-sm-7">
                  <h4>Hướng dẫn làm thủ tục nhập cảnh vào Nhật Bản</h4>
                  <span>20/11/2020</span>
                  <p>Thủ tục nhập cảnh vào Nhật bản là thủ tục bắt buộc phải làm đối với Thủ tục nhập cảnh vào Nhật bản là thủ tục bắt buộc phải làm đối vớiThủ tục nhập cảnh vào Nhật bản là thủ tục bắt buộc phải làm đối vớiThủ tục nhập cảnh vào Nhật bản là thủ tục bắt buộc phải làm đối với ...</p>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
                <div class="d-flex align-items-start">
                  <div class="pic col-sm-5"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
                  <div class="member-info col-sm-7">
                    <h4>Những điều nên làm khi mới sang Nhật</h4>
                    <span>20/11/2020</span>
                    <p>Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mongĐã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mongĐã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mongĐã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong ...</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3" data-aos="zoom-out">
                <div class="d-flex align-items-start">
                  <div class="pic col-sm-5"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
                  <div class="member-info col-sm-7">
                    <h4>Du học Nhật Bản cần bao nhiêu tiền và chi phí gì</h4>
                    <span>20/11/2020</span>
                    <p>I – TỔNG QUAN CHI PHÍ TRƯỚC VÀ SAU DU HỌC NHẬT BẢN 2019 Nhật I – TỔNG QUAN CHI PHÍ TRƯỚC VÀ SAU DU HỌC NHẬT BẢN 2019 Nhật I – TỔNG QUAN CHI PHÍ TRƯỚC VÀ SAU DU HỌC NHẬT BẢN 2019 Nhật I – TỔNG QUAN CHI PHÍ TRƯỚC VÀ SAU DU HỌC NHẬT BẢN 2019 Nhật I – TỔNG QUAN CHI PHÍ TRƯỚC VÀ SAU DU HỌC NHẬT BẢN 2019 Nhật ...</p>
                  </div>
                </div>
              </div>
  
          </div>
          <div class="member ml-3 col-lg-4 col-sm-4 col-xs-12 search">
            <div class="row">

                <div class="section-title col-lg-12 mt-3 mb-3 mr-auto ml-auto p-0">
                    <input type="search" class="search-news search-field mb-0" name="s" value="" id="s" placeholder="Tìm kiếm…" autocomplete="off">
                </div>

                <div class="section-title mt-3 mb-3 mr-auto ml-auto p-0">
                    <h2>Tin tức mới</h2>
                 </div>
                <div class="col-lg-12 mt-2 mb-2">
                    <div class="d-flex align-items-start">
                        <div class="pic col-sm-5"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info col-sm-7 pr-0">
                          <h4 class="new-news left-text">Học viện nhật ngữ SAKURA</h4>
                          <span class="mt-0">20/11/2020</span>
                          <p class="p-new-news left-text">Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2">
                    <div class="d-flex align-items-start">
                        <div class="pic col-sm-5"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info col-sm-7 pr-0">
                          <h4 class="new-news left-text">Giới thiệu trường Nhật ngữ trực thuộc đại học Tokyo</h4>
                          <span class="mt-0">20/11/2020</span>
                          <p class="p-new-news left-text">Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong. Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2">
                    <div class="d-flex align-items-start">
                        <div class="pic col-sm-5"><img src="./img/update3.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info col-sm-7 pr-0">
                          <h4 class="new-news left-text">Trường đại học Kyushu</h4>
                          <span class="mt-0">20/11/2020</span>
                          <p class="p-new-news left-text">Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2">
                    <div class="d-flex align-items-start">
                        <div class="pic col-sm-5"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info col-sm-7 pr-0">
                          <h4 class="new-news left-text">Trường đại học Sophia</h4>
                          <span class="mt-0">20/11/2020</span>
                          <p class="p-new-news left-text">Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2">
                    <div class="d-flex align-items-start">
                        <div class="pic col-sm-5"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info col-sm-7 pr-0">
                          <h4 class="new-news left-text">Trường đại học Kansai Nhật Bản</h4>
                          <span class="mt-0">20/11/2020</span>
                          <p class="p-new-news left-text">Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong Đã sang đến Nhật, gia đình các bạn tại Việt Nam hẳn đang rất mong</p>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </section><!-- End News Section -->

  </main><!-- End #main -->
@include('guest/footer')
@endsection
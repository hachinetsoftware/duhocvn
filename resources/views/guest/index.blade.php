@extends('guest/default')
@section('content')  
@include('guest/navbar')
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container" data-aos="zoom-out" data-aos-delay="100">
      <h1>Chào mừng bạn đến với <span>DuhocVN</span>
      </h1>
      <h2>Nơi tư vấn và tìm kiếm những chương trình du học phù hợp nhất cho bạn</h2>
      <div class="d-flex">
        <a href="https://www.youtube.com/watch?v=eFbytgkFpoc&ab_channel=HachinetAcademy" class="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"> Xem video <i class="icofont-play-alt-2"></i></a>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Featured Services Section ======= -->
    <section id="featured-services" class="featured-services">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4 class="title"><a href="">THÔNG TIN CHÍNH THỐNG, TOÀN DIỆN & KHÁCH QUAN</a></h4>
              <p class="description">Chúng tôi giúp bạn chọn lọc những nội dung chất lượng nhất cho lộ trình du học thuận lợi & thành công!</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4 class="title"><a href="">HỖ TRỢ BẠN TỰ CHUẨN BỊ HÀNH TRANG DU HỌC</a></h4>
              <p class="description">Thật dễ dàng để tự đi du học! Mọi khó khăn của bạn về hồ sơ, xin visa, học bổng … đã có chúng tôi hỗ trợ!</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box" data-aos="fade-up" data-aos-delay="400">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">TƯ VẤN HƯỚNG NGHIỆP QUỐC TẾ</a></h4>
              <p class="description">Nếu chưa được hướng nghiệp, bạn đừng nên đi du học! Bạn cần được định hướng một lộ trình học tập phù hợp trước khi lên</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Featured Services Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Giới thiệu</h2>
          <h3>Về <span>chúng tôi</span></h3>
          <p>DuhocVN là tổ chức tư vấn giáo dục cung cấp dịch vụ hướng nghiệp quốc tế dựa trên nghiên cứu từng cá thể học sinh. Chúng tôi cam kết 100% chuyên gia tư vấn đạt bộ tiêu chuẩn ICC.</p>
        </div>

        <div class="row">
          <div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
            <img src="./img/about.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center justify-text" data-aos="fade-up" data-aos-delay="100">
            <h3>Tại sao nên chọn DuhocVN?</h3>
            <ul>
              <li>
                <i class="bx bx-store-alt"></i>
                <div>
                  <h5>Dịch vụ du học uy tín</h5>
                  <p>Nhận thức rõ rằng việc đi là cả một sự đầu tư lớn đối với phụ huynh và học sinh. Phụ huynh đã phải đầu tư nhiều chi phí và đặt bao niềm tin và hy vọng vào các bạn trẻ. Còn với các bạn, đó là bước ngoặt quan trọng của tương lai, của cuộc đời. Do vậy, chúng tôi chỉ giới thiệu với các bạn danh sách trường tốt nhất mà thôi.</p>
                </div>
              </li>
              <li>
                <i class="bx bx-images"></i>
                <div>
                  <h5>Đào tạo đúng nhu cầu</h5>
                  <p>Bên cạnh giáo trình chính được chọn lọc từ những giáo trình mới và tốt nhất hiện nay, chúng tôi còn đưa vào giảng dạy thêm những giáo trình hỗ trợ phù hợp với mục tiêu khóa học nhằm giúp tăng cường kĩ năng.</p>
                </div>
              </li>
            </ul>
            <p>
              Với đội ngũ cán bộ tư vấn được đạo tạo bàn bản ở nước ngoài cùng nhiều năm kinh nghiệm trong lĩnh vực tư vấn du học, Mona Media luôn mang đến sự hài lòng trên mức mong đợi cho du học sinh, phụ huynh học sinh về chương trình học, trường học cũng như đất nước du học.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-3 col-md-6 d-flex">
            <div class="count-box">
              <i class="icofont-people"></i>
              <span data-toggle="counter-up">1232</span>
              <p>Tổng số khách hàng tại Việt Nam và các nước đã và đang sử dụng dịch vụ.</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-md-0 d-flex">
            <div class="count-box">
              <i class="icofont-pie-chart"></i>
              <span data-toggle="counter-up">521</span>
              <p>Hơn 500 trường đối tác trên khắp thế giới.</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0 d-flex">
            <div class="count-box">
              <i class="icofont-money"></i>
              <span data-toggle="counter-up">1500</span>
              <p>Mức học bổng trung bình mà học sinh nhận được.</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0 d-flex">
            <div class="count-box">
              <i class="icofont-diamond"></i>
              <span data-toggle="counter-up">38</span>
              <p>Hơn 30 sự kiện triển lãm - hội thảo giáo dục hữu ích được tổ chức hàng năm.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Counts Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients section-bg">
      <div class="container" data-aos="zoom-in">

        <div class="row">

          <div class="col-lg-4 col-md-4 col-12 d-flex align-items-center justify-content-center">
            <a href="https://www.netbee.vn" target="_blank"><img src="./img/netbee.png" class="img-fluid" alt=""></a>
          </div>

          <div class="col-lg-4 col-md-4 col-12 d-flex align-items-center justify-content-center">
            <a href="https://www.hachinet.vn" target="_blank"><img src="./img/hachinet.png" class="img-fluid" alt=""></a>
          </div>

          <div class="col-lg-4 col-md-4 col-12 d-flex align-items-center justify-content-center">
            <a href="https://www.devwork.vn" target="_blank"><img src="./img/devworkvn.png" class="img-fluid" alt=""></a>
          </div>

        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Học bổng</h2>
          <h3>Học bổng cho <span>du học</span></h3>
          <p>Khi du học có thành tích tốt các bạn sẽ được cấp học bổng.</p>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="section-title"><img src="./img/update1.jpg" class="img-fluid" alt=""></div>
              <p>Du học nước ngoài và các loại học bổng danh cho du học sinh</p>
              <a href="#">Xem tiếp <span><i class="fa fa-long-arrow-right"></i></span></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="section-title"><img src="./img/update2.jpg" class="img-fluid" alt=""></div>
              <p>Học bổng toàn phần sau đại học với các chuyên ngành khác nhau</p>
              <a href="#">Xem tiếp <span><i class="fa fa-long-arrow-right"></i></span></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="section-title"><img src="./img/update3.jpg" class="img-fluid" alt=""></div>
              <p>Học bổng có giá trị cao tại đại học quốc tế với nhiều loại ngành nghề</p>
              <a href="#">Xem tiếp <span><i class="fa fa-long-arrow-right"></i></span></a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="zoom-in">

        <div class="owl-carousel testimonials-carousel">

          <div class="testimonial-item">
            <img src="./img/img-ceo.jpg" class="testimonial-img" alt="">
            <h3>Hoàng TH</h3>
            <h4>Giám đốc điều hành &amp; Người sáng lập</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Chỉ cần cố gắng thì sẽ có một tương lai tươi sáng và đầy triển vọng.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="./img/atuan.jpg" class="testimonial-img" alt="">
            <h3>Tuấn NT</h3>
            <h4>Trưởng chi nhánh</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Mọi sự cố gắng đều có thành quả xứng đáng.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="./img/ahoangvu.jpg" class="testimonial-img" alt="">
            <h3>Hoàng Vũ</h3>
            <h4>Giám đốc nhân sự</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Có kiến thức, có đam mê sẽ có tất cả.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Action Section ======= -->
    <section id="action" class="action">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Hình ảnh</h2>
          <h3>Một số hình ảnh <span>hoạt động</span></h3>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="action-flters">
              <li data-filter="*" class="filter-active">Tất cả</li>
              <li data-filter=".filter-app">Hội thảo</li>
              <li data-filter=".filter-card">Tin Visa</li>
              <li data-filter=".filter-web">Sự kiện</li>
            </ul>
          </div>
        </div>

        <div class="row action-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-4 col-md-6 action-item filter-app">
            <img src="./img/action1.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-web">
            <img src="./img/action2.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-app">
            <img src="./img/action3.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-card">
            <img src="./img/action4.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-web">
            <img src="./img/action5.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-app">
            <img src="./img/action6.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-card">
            <img src="./img/action7.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-card">
            <img src="./img/action8.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-6 action-item filter-web">
            <img src="./img/action9.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End Action Section -->

    
    <!-- ======= Rate Section ======= -->
    <section id="rates" class="rates">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Đánh giá</h2>
          <h3>Đánh giá từ <span>học viên</span></h3>
        </div>

        <div class="row rates-content">

          <div class="col-lg-6">

            <div class="progress">
              <span class="rate">Đội ngũ giáo viên <i class="val">85%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="rate">Nhân viên phụ trách hoạt động và hỗ trợ học viên <i class="val">78%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

          </div>

          <div class="col-lg-6">

            <div class="progress">
              <span class="rate">Mức độ cải thiện ngôn ngữ <i class="val">75%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="rate">Cơ sở vật chất <i class="val">80%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            </div>

          </div>

        </div>

      </div>
    </section> 
    <!-- End Rate Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>F.A.Q</h2>
          <h3>Câu hỏi <span>thường gặp</span></h3>
        </div>

        <ul class="faq-list" data-aos="fade-up" data-aos-delay="100">

          <li>
            <a data-toggle="collapse" class="" href="#faq1">Mất bao nhiêu tiền để du học tại Mỹ? <i class="icofont-simple-up"></i></a>
            <div id="faq1" class="collapse show" data-parent=".faq-list">
              <p>Chi phí du học Mỹ khác nhau theo từng bậc học (phổ thông, cao đẳng hay đại học), loại hình trường (công lập hay tư thục), vị trí địa lý của trường (thành phố lớn hay nhỏ, ở bang nào), xếp hạng trường (top bao nhiêu). Dưới đây là ước tính chi phí du học Mỹ trung bình mỗi năm (đã gồm học phí + chi phí ăn ở):</p>
              <p>- THPT: Công lập 25,000 USD/năm; tư thục 35,000 USD/năm</p>
              <p>- Cao đẳng cộng đồng: 20,000 USD/năm</p>
              <p>- Đại học: 35,000-45,000 USD/năm tùy trường</p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">Em không giỏi tiếng anh em có theo du học tại Singapore được không? <i class="icofont-simple-up"></i></a>
            <div id="faq2" class="collapse" data-parent=".faq-list">
              <p>
                Tại Singapore, bạn có thể đăng kí học các khóa tiếng Anh từ ngắn hạn đến dài hạn. Và môi trường học tiếng Anh tại Singapore có thể nói rất là tốt cho các bạn muốn học tập trong một môi trường nói tiếng Anh, vì đa phần người dân Singapore sử dụng tiếng Anh như là ngôn ngữ thứ hai. Tại Học viện SIM, EASB, MDIS, ERC hay Dimensions bạn có thể đăng kí các khóa học tiếng Anh trước khi chuẩn bị học các khóa chính.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">Có được phỏng vấn visa lại khi bị trượt không? <i class="icofont-simple-up"></i></a>
            <div id="faq3" class="collapse" data-parent=".faq-list">
              <p>
                Với những bạn đã từng trượt phỏng vấn xin visa du học Mỹ, hoàn toàn có thể xin phỏng vấn lại. Nhưng cần phải chuẩn bị kỹ càng hồ sơ cũng như trả lời thật tốt trong lần phỏng vấn lại nếu bạn không muốn bị rớt nữa.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">Điều kiện du học Mỹ là gì? <i class="icofont-simple-up"></i></a>
            <div id="faq4" class="collapse" data-parent=".faq-list">
              <p>
                - Bậc THPT: THPT của Mỹ bắt đầu từ lớp 9, nghĩa là hết lớp 8 tại Việt Nam học sinh đã có thể vào được năm đầu tiên phổ thông tại Mỹ. Điểm trung bình từ 6.5/10 trở lên, trình độ tiếng Anh tương đương IELTS 5.5 trở lên, một số trường có thể yêu cầu thêm SSAT, TOEFL, SAT…</p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">Thời gian để hoàn tất thủ tục hồ sơ du học Mỹ là bao lâu? <i class="icofont-simple-up"></i></a>
            <div id="faq5" class="collapse" data-parent=".faq-list">
              <p>
                Việc này khác nhau tùy từng hồ sơ, bậc học, trường học mà học sinh lựa chọn. Nhưng tựu chung, tính từ khi hoàn thành toàn bộ giấy tờ cần thiết, thông thường học sinh sẽ mất khoảng 12 tuần để đi tới bước cuối cùng trong quá trình làm hồ sơ. Trong đó, khoảng 2 đến 6 tuần là thời gian xin thư nhập học và từ 4 đến 6 tuần để học sinh luyện phỏng vấn visa và hoàn thiện hồ sơ xin visa. Một số trường yêu cầu nộp hồ sơ trước tháng 1 hoặc tháng 3 cho kỳ nhập học tháng 8 hàng năm, vì thế học sinh sẽ mất thời gian khoảng 8 tháng cho quá trình làm hồ sơ du học. Phụ huynh và học sinh nên có sự chuẩn bị từ sớm để hồ sơ được trau chuốt kỹ lưỡng hơn, tăng khả năng được nhận vào trường và khả năng đỗ visa.
              </p>
            </div>
          </li>

        </ul>

      </div>
    </section><!-- End Frequently Asked Questions Section -->

  </main><!-- End #main -->
@include('guest/footer')
@endsection
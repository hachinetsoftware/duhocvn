
  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top bg-000">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>DuhocVN</h3>
            <p>
              87 Nguyễn Thị Minh Khai <br>
              Thành phố Vinh<br>
              Nghệ An <br><br>
              <strong>SĐT:</strong> +84 912345678<br>
              <strong>Email:</strong> duhocvn@example.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Liên kết nhanh</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href=".">Trang chủ</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="don-hang">Đơn hàng</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="chi-tiet-don-hang">Chi tiết đơn hàng</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="tin-tuc">Tin Tức</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="lien-he">Liên hệ</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Dịch vụ của chúng tôi</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Đăng ký du học</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Thủ tục giấy tờ</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Chính sách hỗ trợ</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tư vấn hướng nghiệp</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tuyển dụng</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Tiện ích</h4>
            <p>Tìm hiểu thêm về chúng tôi thông qua một số mạng xã hội</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="bx bxl-twitter"></i></a>
              <a href="#" class="facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus" data-toggle="tooltip" data-placement="bottom" title="Skype"><i class="bx bxl-skype"></i></a>
              <a href="#" class="linkedin" data-toggle="tooltip" data-placement="bottom" title="LinkedIn"><i class="bx bxl-linkedin"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>
    
  </footer><!-- End Footer -->
<!DOCTYPE html>
<html lang="en">

<head title="head"><title>Du học Việt Nam - Nơi tư vấn, tìm kiếm du học mới nhất</title>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="Du học nước ngoài - Tìm kiếm tin tức du học - Tư vấn du học - Du học nước ngoài - Tìm kiếm tin tức du học - Tư vấn du học" name="description">
  <meta content="Du học nước ngoài - Tìm kiếm tin tức du học - Tư vấn du học" name="keywords">

  <!-- Favicons -->
  <link href="./img/favicon.svg" rel="icon">
  <link href="./img/favicon.svg" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="./vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="./vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="./vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="./vendor/venobox/venobox.css" rel="stylesheet">
  <link href="./vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="./css/style.css" rel="stylesheet">
</head>

<body>
        @yield('content')

<div id="preloader"></div>
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Vendor JS Files -->
<script src="./vendor/jquery/jquery.min.js"></script>
<script src="./vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="./vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="./vendor/php-email-form/validate.js"></script>
<script src="./vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="./vendor/counterup/counterup.min.js"></script>
<script src="./vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="./vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="./vendor/venobox/venobox.min.js"></script>
<script src="./vendor/aos/aos.js"></script>

<!-- Template Main JS File -->
<script src="./js/main.js"></script>

</body>

</html>
